function err = optimize_globaltrans(params, obj1, obj2, match, r)

beta = params(1);
R = SpinCalc('QtoDCM', params(2:5), 0.1, 0);
T = params(6:8)';

deformed_model = deform(beta,obj2,r);
err = [];
labels = unique(deformed_model.labels)'; labels = labels(labels>=0);
for i=1:length(labels)
    ix = deformed_model.labels == labels(i) & ~any(isnan(deformed_model.normal),2);
    projected_vertex = bsxfun(@plus,R*deformed_model.vertex(ix,:)',T)';
    ix = obj1.labels == labels(i) & ~any(isnan(obj1.normal),2);
    err = cat(1,err,projected_vertex(match{i},:)-obj1.vertex(ix,:));
end
