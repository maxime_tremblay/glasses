function h = plot_cam_pos(model,T)
%PLOT_CAM_POS Plot the relative position of the camera views that created
%the model.
%   H = PLOT_CAM_POS(MODEL, T) where MODEL is a struct containing the
%   struct that can be drawn using SHOW_OBJECT(MODEL) and T is a 4x4xN
%   matrix containing all the transformations of the camera; T(4,4,:) == 1.
%
%   Copyright 2016 LVSN

h = show_object(model); hold on;
x = reshape(T(1,4,:),[],2); y = reshape(T(2,4,:),[],2); z = reshape(T(3,4,:),[],2);
u = reshape(T(1,3,:),[],2); v = reshape(T(2,3,:),[],2); w = reshape(T(3,3,:),[],2);
u2 = reshape(T(1,2,:),[],2); v2 = reshape(T(2,2,:),[],2); w2 = reshape(T(3,2,:),[],2);
c = 0:min(size(T,3)-1,256^3-1); c = [c'./256^2, c'./256, mod(c',256)]./255;
for i=1:size(c,1)
    plot3(x(i),y(i),z(i),'Color',c(i,:),'Marker','.');
    quiver3(x(i),y(i),z(i),u(i),v(i),w(i),'Color',c(i,:));
    quiver3(x(i),y(i),z(i),u2(i),v2(i),w2(i),'Color',c(i,[3 2 1]));
end
hold off;