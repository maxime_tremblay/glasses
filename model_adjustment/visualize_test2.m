%open frames, read first one
addpath(genpath('./data/'))
addpath('../3rdparty/spincalc/');
close all
%load kinectcalib
load('calibKinectv2.mat');
Mcext = [0.998377, 0.0440797, 0.0360599, -235.835/1000;
    -0.0435785, 0.998944, -0.0145704, -157.715/1000;
    -0.0366641, 0.0129753, 0.999243, 832.938/1000;
    0, 0, 0, 1];
Mdext = [0.998024, 0.0509706, 0.0367475, -282.523/1000;
    -0.0507772, 0.998691, -0.00617783, -147.664/1000;
    -0.0370143, 0.00429968, 0.999305, 821.814/1000;
    0, 0, 0, 1];
Mext = Mcext/Mdext;

K = 500;
if ~exist('img','var') || ~exist('depth','var')
    if K~=-1
        [img,depth] = kinect.load_color_frames([name '.frames'], K);
    else
        [img,depth] = kinect.load_color_frames([name '.frames']);
    end
end
if ~exist('ix','var')
    ix = kinect.load_indexes([name '.indices']);
    if K~=-1
        ix = ix(ix<K);
    end
end
if ~exist('T','var') || size(T,3)~=K
    if K~=-1
        T = kinect.load_transformations([name '.transforms'], K);
        T = T(:,:,1:length(ix));
    else
        T = kinect.load_transformations([name '.transforms']);
    end
    Rq = SpinCalc('DCMtoQ',T(1:3,1:3,:),0.001,1);
    tq = T(1:3,4,:);
    Tq = [Rq reshape(tq,3,size(tq,3))'];
    Tq2 = zeros(max(ix)+1,size(Tq,2));
    for i=1:size(Tq,2)
        Tq2(:,i) = interp1(ix,Tq(:,i),(0:max(ix))');
    end
    Rdcm = SpinCalc('QtoDCM',Tq2(:,1:4),0.001,1);
    t = reshape(Tq2(:,5:end)',3,1,[]);
    T = cat(1,cat(2,Rdcm,t),repmat([0 0 0 1],1,1,size(Tq2,1)));
    ix = 0:max(ix);
    assert(length(ix)==size(T,3));
end

projected_deformed2 = projected_deformed;
face2 = face;
vw = VideoWriter([name '_depth_vertex_sidebyside_500.mp4'], 'MPEG-4');
open(vw);
a = eye(3); a(3,3) = -1; a(1,1) = -1; 
projected_deformed2.vertex = projected_deformed2.vertex*a;
face2.vertex = face2.vertex*a;
offset = [0 0 0]'; %[0 -0.022 -0.02]';
for i=1:K  
    Tp = T(:,:,i);
    R = Tp(1:3,1:3);
    t = Tp(1:3,4);
    
    mean_vertex = mean([projected_deformed2.vertex;face2.vertex]);
    tmp_glasses = projected_deformed2; 
    tmp_glasses.vertex = bsxfun(@minus,tmp_glasses.vertex, mean_vertex); 
    tmp_glasses.vertex = cat(2,tmp_glasses.vertex,ones(size(tmp_glasses.vertex,1),1))*Tp; 
    tmp_glasses.vertex = tmp_glasses.vertex(:,1:3);
    tmp_glasses.vertex = bsxfun(@plus,tmp_glasses.vertex,mean_vertex); 
    tmp_face = face2; 
    tmp_face.vertex = bsxfun(@minus,tmp_face.vertex,mean_vertex); 
    tmp_face.vertex = cat(2,tmp_face.vertex,ones(size(tmp_face.vertex,1),1))*Tp; 
    tmp_face.vertex = tmp_face.vertex(:,1:3);
    tmp_face.vertex = bsxfun(@plus,tmp_face.vertex,mean_vertex);
    new_img = visualize(390,eye(3),[0 0 0]',tmp_glasses,tmp_face,repmat(uint8(mod(depth(:,:,i),256)),1,1,3),[0 0 1],true,false);
    
    %new_img = visualize(390,R,t+offset,face2,[],repmat(uint8(mod(depth(:,:,i),256)),1,1,3),[0 0 1],true,false);
    
    %new_img = visualize(mean([Fx,Fy]),R,t,projected_deformed2,face2,img(:,:,:,i),[0 0 1],true,false);
    %new_img = imresize(new_img,1/2);
    
    writeVideo(vw,new_img);
    fprintf(' %i',i);
    if mod(i,20)==0
        fprintf('\n');
    end
end
fprintf('\n');
close(vw);