function img = undistortImage(img, camera, varargin)
% Applies the radial distortion parameters estimated by SfM to an image. 
%
%   img = undistortImage(img, camera, ...)
%
% See also:
%   distortImage
%
% ----------
% Jean-Francois Lalonde

% simply call distortImage with 'undistort' set to true!
img = distortImage(img, camera, 'undistort', true, varargin{:});