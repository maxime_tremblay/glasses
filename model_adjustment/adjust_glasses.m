function [params, adjusted_glasses, residual] = adjust_glasses(face, glasses, r_axes, R, T, opts)
% ADJUST_GLASSES Finds the parameters that put the glasses model on the face
% model
%   [PARAMS, ADJUSTED_GLASSES, RESIDUAL] = ADJUST_GLASSES(FACE, ... 
%   GLASSES, R_AXES, R, T) Finds the parameters PARAMS to put the GLASSES
%   model in the FACE model considiring the glasses start at translation T
%   and rotation R. R_AXES indicates where the glasses branches can be
%   rotated. The ADJUSTED_GLASSES model is return and the RESIDUAL from the
%   alignement optimization as well. By default, only the rotation and
%   translation are researched, so the R_AXES parameters is more or less
%   useless.
%
%   [PARAMS, ADJUSTED_GLASSES, RESIDUAL] = ADJUST_GLASSES(FACE, ... 
%   GLASSES, [R_AXES], R, T, OPTS) Same as above, but OPTS contains
%   different options 'FIELD', 'VALUE'. These fields and value are as
%   following:
%
%     Field         Value
%     'tol'         Tolerated variation between two iterations of the
%                   algorithm [0,1]. Default value: 0.001.
%
%     'nstep_max'   Maximum number of iterations to optimize the requested
%                   transformation. Between each iteration, a new set of 
%                   nearest neighbors are calculated. Default value: 15.
%
%     'trans'       Boolean parameters indicating that the user wants the
%                   rotation and translation that fit the glasses on the 
%                   face. Default value: True.
%
%     'deform'      Boolean parameters indicating that the user wants the
%                   glasses' branches roation that fit the glasses on the 
%                   face. Default value: False.
%
%     'clip'        Boolean parameters indicating that the user wants the
%                   no clipping term added to the energy function to 
%                   minimize. Needs clip and deform activated to work. 
%                   Default value: False.
%
%     'lambda'      Float parameters that weight the no clipping term with 
%                   the distance term in the optimization function. Needs 
%                   clip activated to mean something. Default value: 1.
%
%     'init_params' Starting point of the energy function. 
%                   Params = [theta, Q1, Q2, Q3, Q4, T1, T2, T3], where
%                   theta is the glasses' branches rotation angle in
%                   degrees, Q1-4 is the rotation matrix in quaternion
%                   form, and T1-3 is the translation matrix. Default
%                   value: [0 DCMtoQ(R) T'];
%
%     'verbose'     Verbose parameters. Default value: False.
%
%   See LABEL_MODEL_TEST 
%   2016 Maxime Tremblay

%Need this to transform Quaternion to/from DCM (Rotation Matrix)
addpath('../3rdparty/SpinCalc/');

if ~exist('opts','var'), opts = struct(); end
if ~isfield(opts,'tol'), opts.tol = 0.001; end
if ~isfield(opts,'nstep_max'), opts.nstep_max = 15; end
if ~isfield(opts,'trans'), opts.trans = true; end
if ~isfield(opts,'deform'), opts.deform = false; end
if ~isfield(opts,'clip'), opts.clip = false; end
if ~isfield(opts,'lambda'), opts.lambda = 1; end
if ~isfield(opts,'init_params') 
    % Rotation is optimized in quaternion form since 1) there is less
    % parameters to optimize than in rotation matrix form and 2) they are 
    % more numerically stable.
    opts.init_params = [0 SpinCalc('DCMtoQ', R, 0.1, 0) T']; 
elseif length(opts.init_params)==1
    opts.init_params = [opts.init_params SpinCalc('DCMtoQ',R, 0.1, 0) T']; 
end
if ~isfield(opts,'verbose'), opts.verbose = false; end

params = opts.init_params;
variation = inf; nstep = 0; last_resnorm = 0;

while(variation > opts.tol && nstep < opts.nstep_max)
    if opts.verbose
        if opts.deform
            fprintf('theta: %2.4f ', params(1));
        end
        if opts.trans
            angles = SpinCalc('QtoEA321',params(2:5), 0.1, 0);
            yaw = angles(1); pitch = angles(2); roll = angles(3);
            fprintf('rotation (yaw-pitch-roll): %2.2f, %2.2f, %2.2f, translation: %2.2f, %2.2f, %2.2f ', [yaw, pitch, roll, params(6:8)]);
        end
        if opts.trans || opts.deform
            fprintf('\n');
        end
    end
    if opts.trans
        R = SpinCalc('QtoDCM', params(2:5), 0.1, 0);
        T = params(6:8)';
    end
    
    % We need to find, for this iteration, the nearest neighbor of every
    % labeled vertices; the optimization process will be carried on these
    % matches.
    if opts.deform
        model_to_match = deform(params(1), glasses, r_axes);
    else
        model_to_match = glasses;
    end
    labels = unique(model_to_match.labels)'; labels = labels(labels>=0);
    match = cell(length(labels),1);
    for i=1:length(labels)
        ix = model_to_match.labels == labels(i) & ~any(isnan(model_to_match.normal),2);
        aligned_vertex = bsxfun(@plus,R*model_to_match.vertex(ix,:)',T)';
        kdOBJ = KDTreeSearcher(aligned_vertex);
        match{i} = knnsearch(kdOBJ,face.vertex(face.labels==labels(i) & ~any(isnan(face.normal),2),:));
    end
    
    % trans, deform and clip options determine how the glasses model
    % will be modified/transformed to fit the face model.
    % optimize_transformation: means only translation + rotation
    % optimize_deformation: means only glasses branch rotation
    % optimize_globaltrans: means translation + rotation + branch rotation
    % optimize_globaltrans_clip: means everything above + the term that 
    % prevents clipping; this last one is solved differently since
    % least-square non-linear can not solve the equation
    if opts.trans && ~opts.deform
        o = @(p) optimize_transformation(p, face, glasses, match);
        [params(2:end), resnorm, residual] = lsqnonlin(o, params(2:end), [], [], optimoptions('lsqnonlin','Display','off'));        
    elseif ~opts.trans && opts.deform
        o = @(theta) optimize_deformation(theta, face, glasses, match, R, T, r_axes);
        [params(1), resnorm, residual] = lsqnonlin(o, params(1), [], [], optimoptions('lsqnonlin','Display','off'));
    else
        if ~opts.clip
            o = @(p) optimize_globaltrans(p, face, glasses, match, r_axes);
            [params, resnorm, residual] = lsqnonlin(o, params, [], [], optimoptions('lsqnonlin','Algorithm','Levenberg-Marquardt','Display','off'));
        else
            o = @(p) optimize_globaltrans_clip(p, face, glasses, match, opts.lambda, r_axes);
            [params, resnorm, residual] = fminunc(o, params, optimoptions('fminunc','Display','off','Algorithm','quasi-newton'));
        end
    end
    % variation threshold value is calculated in percent from the last
    % resorm (residual norm)
    variation = abs(last_resnorm-resnorm)/resnorm; last_resnorm = resnorm;
      
    nstep = nstep+1;
    if opts.verbose
        fprintf('variation: %2.4f, resnorm: %2.4f, nstep: %d\n', variation, resnorm, nstep);
    end
end
if ~opts.trans && opts.deform
    R = SpinCalc('QtoDCM',params(2:5), 0.1, 0);
    T = params(6:8)';
end
adjusted_glasses = deform(params(1), glasses, r_axes);
adjusted_glasses.vertex = bsxfun(@plus, R * model_to_match.vertex', T)';