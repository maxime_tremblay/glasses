%deform_test
close all
%addpath('./data/2016.02.15 (Frames, Mirrored Mesh, and Transforms)/');
addpath('./data/face/Frames20160118/');
addpath('./data/glasses/');
addpath('../3rdparty/plyread/');
addpath('../3rdparty/sovarux/');
addpath('../3rdparty/SpinCalc/');

%% Load mesh data and preselected feature
name = '003m';
if ~exist('face','var')
    filename = [name '.mat'];
   if ~exist(filename,'file')
        face = load_data([name '.ply'], ...
            {[name '_nosearc.ply'], ...
            [name '_leftear.ply'], ...
            [name '_rightear.ply']});
        save(filename,'face');
   else
       load(filename,'face');
   end
end
filename = 'glasses_nolens.mat';
if ~exist(filename,'file')
    glasses_parts = load_data('glasses_nolens.ply', ...
        {'glasses_nosearc_2.ply', 'glasses_leftbranch.ply', 'glasses_rightbranch.ply'});
    save(filename,'glasses_parts');
else
   load(filename,'glasses_parts');
end

%% Data manipulation (rescale glasses to be in the same scale as the face, place them closer to the face so the icp works better)
s = [1/1000 1/1250 1/1000];
glasses_parts = rescale_model(glasses_parts,s);
r(1) = struct('axis',{{34177,[0 1 0]}},'label',2); 
r(2) = struct('axis',{{352,[0 -1 0]}},'label',1); 


%% first model adjustment phase + display
[projected_glasses, ER, R, T] = model_adjustment(face, glasses_parts, true);
show_adjusted_glasses(face, projected_glasses);

%% adjust_glasses (deformation only) + display
[params, deformed_model] = adjust_glasses(face, glasses_parts, r,R,T,true,struct('init_params',0,'verbose',true));
theta = params(1);
show_adjusted_glasses(face, deformed_model);

%% adjust glasses (deformation + rot + trans + clip check) + display 
[params, projected_deformed, residual] = adjust_glasses(face, glasses_parts, r, R, T, false, struct('init_params',theta,'verbose',true,'clip',true,'lambda',0.001,'nstep_max',15,'tol',0.001));
show_adjusted_glasses(face, projected_deformed);

%% show model clipping through the other model
show_clipping(face, projected_deformed, '.r');