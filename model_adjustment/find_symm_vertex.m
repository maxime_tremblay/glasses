function idx_pair = find_symm_vertex(vertex, s_axe)

idx_pair = [1:size(vertex,1); zeros(1,size(vertex,1))]';
dist = zeros(size(vertex,1),1);
for i=1:size(vertex,1)
    idx = [1:i-1, i+1:size(vertex,1)];
    [dist(i),idx_pair(i,2)] = min(sum(bsxfun(@minus, -vertex(idx,:), vertex(i,:)).^2));
end

