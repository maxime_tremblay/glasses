function [h, err] = show_clipping(face, glasses, str_opts)
% SHOW_CLIPPING shows which vertex from the glasses model clip through the
% face model
%   
%   SHOW_CLIPPING(FACE, GLASSES) Looked at every pair of nearest labeled
%   pixels from FACE and GLASSES 3d model and shows any glasses vertexes 
%   that clipped through their face vertex conterpart. By default, those
%   vertex are shown as red dot.
%
%   SHOW_CLIPPING(FACE, GLASSES, STR_OPTS) As above, STR_OPTS let the user
%   change the marker of the clipped vertexes.
%
%   2016 Maxime Tremblay

if nargin < 3
    str_opts  = '.r';
end

labels = unique(glasses.labels)'; labels = labels(labels>=0);
err = [];
vertex = cell(1,length(labels));
for i=1:length(labels)
    aligned_vertex = glasses.vertex(glasses.labels==labels(i),:);
    kdOBJ = KDTreeSearcher(aligned_vertex);
    nok = face.labels==labels(i) & ~any(isnan(face.normal),2);
    face_vertex = face.vertex(nok,:); face_normal = face.normal(nok,:);
    match = knnsearch(kdOBJ,face_vertex);
    vertex{i} = aligned_vertex(match,:);
    d = vertex{i}-face_vertex; d = d./repmat(sqrt(sum(d.^2,2)),1,3);
    err = cat(1,err,1-sig(sum(d.*face_normal,2),10,-3));
end
vertex = vertcat(vertex{:});
err_idx = err>0.9;

h = figure; 
show_object(face, min(1, 170 * [ 1.5 1.15 1.0 ]/255), [0 0 -1]);
hold on;
show_object(glasses, [0.7 0.7 0.7]);
plot3(vertex(err_idx,1),vertex(err_idx,2),vertex(err_idx,3), str_opts);
hold off;
cameratoolbar('show');
cameratoolbar('setMode','orbit');
camzoom(1.5);
axis vis3d equal off ij
drawnow;