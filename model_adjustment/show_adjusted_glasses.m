function h = show_adjusted_glasses(face, glasses, model)

if nargin<3
    views = { [ 1 0.1 -0.1 ], [ 0 0 -1 ], [-1 0.1 -0.45 ] };
    rolls = [ -90, 0, 90 ];
else
    [views, rolls] = find_views(model);
end
glasses_tri = triangulation(glasses.vertex_indices, glasses.vertex(:,1), glasses.vertex(:,2), glasses.vertex(:,3));
face_tri = triangulation(face.vertex_indices, face.vertex(:,1), face.vertex(:,2), face.vertex(:,3));
h = figure;
pgShowSurface3( face_tri, struct('hold','on', 'views', {views}, 'rolls', rolls ) ); 
pgShowSurface3( glasses_tri, struct('color', [0.75 0.75 0.75] ) );
drawnow;