function scan = label_model(scan, model, model_ix, varargin)
%LABEL_MODEL Add labels to the scan using the generic model
%   SCAN = LABEL_MODEL(SCAN, MODEL, MODEL_IX,
%   'NAME1',VALUE1,...,'NAMEN',VALUEN) finds the nearest neighbor vertex of
%   MODEL_IX from MODEL in SCAN using different NAME and VALUE options.
%   Returns the updated SCAN struct.
%
%     Name          Value
%     'K'           A positive integer, K, specifying the number of nearest
%                   neighbors in scan to find for each point in model_ix.
%                   Default is 1. K can also be a vector of different
%                   positive integer of Nx1.
%
%     'dist'    Nearest neighbors maximum distance (in the same unit as the
%               vertex in scan and model). dist can also be a vector of
%               different distance of Nx1.
% 
%   Example:
%      % Find 50 nearest neighbor of the points 37,97,126 of the model;
%      % these 50 points shoud be within a eucledean distance of 0.01
%      scan = label_model(fusion_face, model_face, [37, 97, 126], 'dist', 0.01, 'K', 50);
%
%   Copyright 2016 LVSN


if isempty(varargin)
    warning('You should put K and/or dist option value');
end
opts = struct();
for i=1:2:length(varargin)
    if strcmp(varargin{i},'dist')
        opts.dist = varargin{i+1};
    elseif strcmp(varargin{i},'K')
        opts.K = varargin{i+1};
    else
        error('opts %s is not valid',varargin{i});
    end
end
if ~isfield(opts, 'dist'); opts.dist = inf; end;
if ~isfield(opts, 'K'); opts.K = 1; end;

nbfeat = length(model_ix);
match = cell(nbfeat,1);

if length(opts.dist)==1; opts.dist = repmat(opts.dist,nbfeat,1); end;
if length(opts.K)==1; opts.K = repmat(opts.K,nbfeat,1); end;

for i = 1:nbfeat
    if ~isinf(opts.dist)
        dist = sum(bsxfun(@minus, scan.vertex, model.vertex(model_ix(i),:)).^2,2);
        ix = find(dist<=opts.dist(i));
    else
        ix = 1:size(scan.vertex,1);
    end
    scan_vertex = scan.vertex(ix,:);
    kdOBJ = KDTreeSearcher(scan_vertex);
    match{i} = knnsearch(kdOBJ, model.vertex(model_ix(i),:), 'K', min(size(scan_vertex,1),opts.K(i)));
    scan.labels(ix(match{i})) = i-1;
end