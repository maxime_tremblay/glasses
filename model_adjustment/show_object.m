function h = show_object(model, color, light_pos, show_labels)
%SHOW_OBJECT Show a 3D model
%
%   H = SHOW_OBJECT(MODEL) Show the struct MODEL and return figure handle
%   H. No light, no color.
%
%   H = SHOW_OBJECT(MODEL, COLOR) Same as above, but with a rgb
%   COLOR (1x3 vector).
%
%   H = SHOW_OBJECT(MODEL, COLOR, LIGHT_POS) Same as above, but with a
%   directionnal light at LIGHT_POS position (vector of 1x3).
%
%   H = SHOW_OBJECT(MODEL, COLOR, LIGHT_POS, SHOW_LABELS) Same as above,
%   but SHOW_LABELS is a bool parameters; it shows every labeled vertexes.
%
%   2016 Maxime Tremblay

if nargin<4
    show_labels = false;
end

model_tri = triangulation(model.vertex_indices, ...
    model.vertex(:,1), model.vertex(:,2), model.vertex(:,3));
h = gcf;
if nargin >= 2
    trisurf( model_tri, 'EdgeColor', 'None', 'FaceColor', color);
    if nargin >= 3
        if isempty(light_pos)
            light_pos = [0 -0.75 -1];
        end
        light('Position',light_pos,'Style','infinite');
    end  
    material([0.25 0.6 .25 7]);
else
    trisurf( model_tri, 'EdgeColor', 'None');
end
axis vis3d equal ij
xlabel('x'); ylabel('y'); zlabel('z');

if show_labels
    hold on;
    c = colormap('lines');
    labels = unique(model.labels(model.labels~=-1));
    for i=1:length(labels)
        ix = model.labels == labels(i);
        plot3(model.vertex(ix,1),model.vertex(ix,2),model.vertex(ix,3),'MarkerEdgeColor',c(i,:),'Marker','.','LineStyle','none');
    end
    hold off;
end

view( [0 0 -1] );
camroll(0);
cameratoolbar('show');
cameratoolbar('setMode','orbit');
axis vis3d equal off ij
drawnow;