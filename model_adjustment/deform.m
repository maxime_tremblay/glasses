function [deformed_model, T, R] = deform(theta, model, r_axes)
%DEFORM Apply a partial deformation to a model
%   [DEFORMED_MODEL, T, R] = DEFORM(THETA, MODEL, R_AXES) Apply a rotation 
%   of THETA degrees on vertices from MODEL struct VERTEX field. The 
%   rotations information are contained inside R_AXES struct; the field 
%   LABEL contains the label on which each rotations are applied, the field 
%   AXIS contains cells: the first one contains the vertex index on which 
%   the rotation is centered and the second one contains the axis on which 
%   the rotation is applied.
%
%   See also DEFORM_TEST, DEFORM_TEST_SHORT
%
%   2016 Maxime Tremblay

addpath('../3rdparty/SpinCalc/');
deformed_model = model;
T = cell(length(r_axes),1); R = cell(length(r_axes),1);
theta = wrapTo360(theta);
for i=1:length(r_axes)
    r_axis = r_axes(i).axis{2};
    ix = model.labels==r_axes(i).label;
    vertex = model.vertex(ix,:); 
    
    %calculate rotation transformation to apply to each branches
    R{i} = SpinCalc('EVtoDCM', [r_axis/norm(r_axis), theta], 0.01, 1);

    %translate each branches at the origin
    T{i} = model.vertex(r_axes(i).axis{1},:);% + (r(i).axis{2}/norm(r_axis))/2;
    vertex = bsxfun(@minus,vertex,T{i});

    %rotate
    vertex = R{i}*vertex';

    %return to original position
    vertex = bsxfun(@plus,vertex',T{i});
    
    deformed_model.vertex(ix,:) = vertex;
end