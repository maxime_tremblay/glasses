function save_clipping(filename, face, glasses, plot_str, title_str, show)
% SAVE_CLIPPING function to save 3D model(s) in a 2D image format
%
%   SAVE_MODEL(FILENAME, FACE, GLASSES) Save a FILENAME png image of the 
%   FACE and GLASSES 3D model. The function takes care to add empty space 
%   which is [240, 240, 240] an alpha value of 1. It shows where the
%   glasses model clipped through the face model by plotting marker.
%
%   SAVE_MODEL(FILENAME, FACE, GLASSES, PLOT_STR) As above, but it plot 
%   specific PLOT_STR marker.
%
%   SAVE_MODEL(FILENAME, FACE, GLASSES, PLOT_STR, TITLE_STR) As above, 
%   but also add a title TITLE_STR to the saved figure.
%
%   SAVE_MODEL(FILENAME, FACE, GLASSES, PLOT_STR, TITLE_STR, SHOW) As 
%   above, but also show the result in a figure.
%
%   2016 Maxime Tremblay
if nargin < 4 || isempty(plot_str)
    plot_str = 'xc';
end
h = show_clipping(face, glasses, plot_str); 
if nargin >= 5
    title(title_str);
end
if nargin < 6
    show = false;
end

res = get(0, 'screensize'); pos = [0 0 res(3) res(4)]; set(h, 'Position', pos);
img = getframe(h);
vec_img = reshape(img.cdata,[],3); 
alpha = 1-double(reshape(all(bsxfun(@eq,vec_img,[240  240  240]),2),size(img.cdata,1),size(img.cdata,2)));

if ~isempty(filename)
    imwrite(img.cdata, filename, 'Alpha', alpha, 'Background', [0 0 0]);
end
if ~show
    close(h);
end