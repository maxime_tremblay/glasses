function plotMesh(x,y,z)
h = gcf; figure(h);
for i=1:size(x,2)-1
    plot3(x(i:i+1),y(i:i+1),z(i:i+1)); hold on;
end