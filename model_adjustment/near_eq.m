function c = near_eq(a,b)
%NEAR_EQ(A,B) Compares two float A and B and see if there near enough and
%returns a bool value C
c = abs(single(a) - single(b)) <= eps(single(a));