function w = weight(idx,rp,rq,normalize)

if (nargin<4)
    normalize = false;
end

w = double(rp(idx) == rq);
%pondere! avec la taille de rp,rq
if(normalize)
    for i = unique(rq)
        ix = find(rq == i);
        w(ix) = w(ix)*length(ix)/length(w);
    end
end


