%open frames, read first one
addpath(genpath('./data/'));
addpath('../3rdparty/spincalc/');
close all
%load kinectcalib
load('calibKinectv2.mat');
Mcext = [0.998377, 0.0440797, 0.0360599, -235.835/1000;
    -0.0435785, 0.998944, -0.0145704, -157.715/1000;
    -0.0366641, 0.0129753, 0.999243, 832.938/1000;
    0, 0, 0, 1];
Mdext = [0.998024, 0.0509706, 0.0367475, -282.523/1000;
    -0.0507772, 0.998691, -0.00617783, -147.664/1000;
    -0.0370143, 0.00429968, 0.999305, 821.814/1000;
    0, 0, 0, 1];
Mext = Mcext/Mdext;


K = 400; 
interpolate = false; %should be false when a lot of frames where lost
if ~exist('img','var') || ~exist('depth','var')
    if K~=-1
        [img,depth] = kinect.load_color_frames([name '.frames'], K);
    else
        [img,depth] = kinect.load_color_frames([name '.frames']);
    end
end
K = min(size(img,4),K);
if ~exist('ix','var')
    ix = kinect.load_indexes([name '.indices']);
    if K~=-1
        ix = ix(ix<K);
    end
end
if ~exist('T','var')
    if K~=-1
        T = kinect.load_transformations([name '.transforms'], K);
        T = T(:,:,1:length(ix));
    else
        T = kinect.load_transformations([name '.transforms']);
    end
    if interpolate
        Rq = SpinCalc('DCMtoQ',T(1:3,1:3,:),0.001,0);
        tq = T(1:3,4,:);
        Tq = [Rq reshape(tq,3,size(tq,3))'];
        Tq2 = zeros(max(ix)+1,size(Tq,2));
        for i=1:size(Tq,2)
            %perhaps (K-1) should repace max(ix), BUT if there is more than one
            %or two point missing at the end of the data, we shoud not
            %EXTRAPOLATE transform (chose a different K, perhaps?)
            Tq2(:,i) = interp1(ix,Tq(:,i),(0:max(ix))');
        end
        Rdcm = SpinCalc('QtoDCM',Tq2(:,1:4),0.001,1);
        t = reshape(Tq2(:,5:end)',3,1,[]);
        T = cat(1,cat(2,Rdcm,t),repmat([0 0 0 1],1,1,size(Tq2,1)));
        ix = 0:max(ix);
    end
    assert(length(ix)==size(T,3));
end

vw = VideoWriter([name '_depth_vertex_' sprintf('%i',K) '.mp4'], 'Uncompressed AVI');
open(vw);
a = 1:length(ix);
face2 = face; face2.vertex(:,1) = -face2.vertex(:,1);
projected_deformed2 = projected_deformed; projected_deformed2.vertex(:,1) = -projected_deformed2.vertex(:,1);
for i=a
    Tp = T(:,:,i);
    R = Tp(1:3,1:3);
    t = Tp(1:3,4);
    
    %http://www.int-arch-photogramm-remote-sens-spatial-inf-sci.net/XL-5-W4/93/2015/isprsarchives-XL-5-W4-93-2015.pdf
    %mean([389.436,391.366]) --> in the offficial doc: 366,0 
    new_img = visualize(390,R,t,projected_deformed2,face2,repmat(uint8(mod(depth(:,:,ix(i)+1),256)),1,1,3),[0 0 -1],false,false);
    
%     Tp = T(:,:,i)*Mext;
%     R = Tp(1:3,1:3);
%     t = Tp(1:3,4);
%     new_img = visualize(mean([Fx,Fy])*1.25,R,t,projected_deformed,face,img(:,:,:,ix(i)+1),[0 0 -1],false,true);
        
    writeVideo(vw,new_img);
    fprintf(' %i',i);
    if mod(i,20)==0
        fprintf('\n');
    end
end
fprintf('\n');
close(vw);