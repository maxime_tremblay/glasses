function model = rescale_model(model, s)
% RESCALE_MODEL Rescale the vertex of a 3D model
%   
%   MODEL = RESCALE_MODEL(MODEL, S) Rescale the MODEL struct VERTEX field with a 
%   scale factor of S. S can be a scalar or a 1x3 vector for different
%   scaling factor in x,y,z.
%
%   2016 Maxime Tremblay


if length(s)==1
    S = [s 0 0; 0 s 0; 0 0 s];
else
    S = [s(1) 0 0; 0 s(2) 0; 0 0 s(3)];
end
model.vertex = model.vertex*S;