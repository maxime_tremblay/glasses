#include <lvsn/kinect/client>
#include <mex.h>
#include <stdexcept>
#include <string>
#include <chrono>
#include <tuple>
#include <memory>

using namespace lvsn::kinect;
using namespace std;

tuple<string,string,size_t,chrono::seconds,verbose> parse_input();

void mexFunction(int nb_outputs,mxArray *outputs[],int nb_inputs,const mxArray *inputs[]){
    try{
        ::mexAtExit([](){
        });
        auto name          =string("kinect client");
        auto server_address=string("bazin.gel.ulaval.ca:1234");
        auto nb_buffers    =size_t(3);
        auto timeout       =chrono::seconds(10);
        switch(nb_inputs){
            case 0:
            case 1:
                throw client::error_type("...");
            case 2:
            case 3:
            case 4:
                break;
            default:
                break;
        }
        client c(name,server_address,nb_buffers,chrono::seconds(timeout),verbose::silent);
        c.connect();
        c.disconnect();

        mexPrintf("\nThere are %d right-hand-side argument(s).", nb_inputs);
        for(int i=0; i<nb_inputs; i++){
            mexPrintf("\n\tInput Arg %i is of type:\t%s ",i,mxGetClassName(inputs[i]));
        }

        mexPrintf("\n\nThere are %d left-hand-side argument(s).\n", nb_outputs);
        if(nb_outputs>nb_inputs)
            throw runtime_error("cannot specify more outputs than inputs");

        for(int i=0; i<nb_outputs; i++){
            outputs[i]=mxCreateDoubleMatrix(1,1,mxREAL);
            *mxGetPr(outputs[i])=(double)mxGetNumberOfElements(inputs[i]);
        }
    }
    catch(const exception& e){
        const auto& function_name=::mexFunctionName();
        mexErrMsgIdAndTxt("MATLAB:mexfunction:inputOutputMismatch",e.what());
    }
    catch(...){
        const auto& function_name=::mexFunctionName();
        mexErrMsgIdAndTxt("MATLAB:mexfunction:inputOutputMismatch","unhandled exception");
    }
}
