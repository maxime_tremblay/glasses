#include <mex.h>
#include <stdexcept>
#include <string>
#include <chrono>
#include <tuple>
#include <vector>
#include <algorithm>

#include <Kinect.h>
#pragma comment (lib,"kinect20.lib")

using namespace std;

const auto syntax_error_message=string("the function argument must be an Mx3 array of x-,y-, and z-coordinates of 3D points in the camera space");

mxArray const*const parse_arguments(const int nb_inputs,const mxArray *inputs[]);
mxArray* camera_space_points_to_color_space_points(mxArray const*const src);

void mexFunction(int nb_outputs,mxArray *outputs[],int nb_inputs,const mxArray *inputs[]){
    try{
        const auto& camera_points=inputs[0];
        if(!::mxIsNumeric(camera_points))
            throw runtime_error("the function argument must be a numerical array");
        if(::mxGetNumberOfDimensions(camera_points)!=2||::mxGetDimensions(camera_points)[1]!=3)
            throw runtime_error(syntax_error_message);
        outputs[0]=camera_space_points_to_color_space_points(camera_points);
    }
    catch(const exception& e){
        const auto& function_name=::mexFunctionName();
        mexErrMsgIdAndTxt("MATLAB:mexfunction:inputOutputMismatch",e.what());
    }
    catch(...){
        const auto& function_name=::mexFunctionName();
        mexErrMsgIdAndTxt("MATLAB:mexfunction:inputOutputMismatch","unhandled exception");
    }
}

template<typename T>
vector<CameraSpacePoint> to_camera_space_points(mxArray const*const points){
    const auto ndim=::mxGetNumberOfDimensions(points);
    const auto& dim=::mxGetDimensions(points);
    if(ndim!=2||dim[1]!=3)
        throw runtime_error(syntax_error_message);
    const auto& nb_points=size_t(dim[0]);
    auto result=vector<CameraSpacePoint>(nb_points);
    auto itx=(T*)::mxGetData(points);
    auto ity=itx+nb_points;
    auto itz=ity+nb_points;
    for_each(result.begin(),result.end(),[&](CameraSpacePoint& p){
        p={FLOAT(*itx++),FLOAT(*ity++),FLOAT(*itz++)};
    });
    return result;
}

template<typename T>
mxArray* from_color_space_points(const vector<ColorSpacePoint>& points){
    mxClassID classid;
    if     (is_same<T,float>::value)    classid=mxSINGLE_CLASS;
    else if(is_same<T,double>::value)   classid=mxDOUBLE_CLASS;
    else if(is_same<T,int64_t>::value)  classid=mxINT64_CLASS;
    else if(is_same<T,uint64_t>::value) classid=mxUINT64_CLASS;
    else if(is_same<T,int32_t>::value)  classid=mxINT32_CLASS;
    else if(is_same<T,uint32_t>::value) classid=mxUINT32_CLASS;
    else if(is_same<T,int16_t>::value)  classid=mxINT16_CLASS;
    else if(is_same<T,uint16_t>::value) classid=mxUINT16_CLASS;
    else if(is_same<T,int8_t>::value)   classid=mxINT8_CLASS;
    else if(is_same<T,uint8_t>::value)  classid=mxUINT8_CLASS;
    else
        throw runtime_error("unsupported data type");
    const auto& nb_points=points.size();
    const mwSize dim[2]={mwSize(nb_points),2};
    const auto result=::mxCreateNumericArray(2,dim,classid,mxREAL);
          auto itx=(T*)::mxGetData(result);
          auto ity=itx+nb_points;
    for_each(points.cbegin(),points.cend(),[&](const ColorSpacePoint& p){
        *itx++=p.X;
        *ity++=p.Y;
    });
    return result;
}

mxArray* camera_space_points_to_color_space_points(mxArray const*const src){
    IKinectSensor           *sensor                  =nullptr;
    ICoordinateMapper       *coordinate_mapper       =nullptr;
    IMultiSourceFrameReader *multisource_frame_reader=nullptr;
	IMultiSourceFrame       *multisource_frame       =nullptr;
	IColorFrameReference    *color_frame_reference   =nullptr;
	IColorFrame             *color_frame             =nullptr;
	IDepthFrameReference    *depth_frame_reference   =nullptr;
	IDepthFrame             *depth_frame             =nullptr;
    const auto& release_all_interfaces=[&](){
		if(depth_frame!=nullptr){
			depth_frame->Release();
			depth_frame=nullptr;
		}
		if(depth_frame_reference!=nullptr){
			depth_frame_reference->Release();
			depth_frame_reference=nullptr;
		}
		if(color_frame!=nullptr){
			color_frame->Release();
			color_frame=nullptr;
		}
		if(color_frame_reference!=nullptr){
			color_frame_reference->Release();
			color_frame_reference=nullptr;
		}
		if(multisource_frame!=nullptr){
			multisource_frame->Release();
			multisource_frame=nullptr;
		}
        if(multisource_frame_reader!=nullptr){
            multisource_frame_reader->Release();
            multisource_frame_reader=nullptr;
        }
        if(coordinate_mapper!=nullptr){
            coordinate_mapper->Release();
            coordinate_mapper=nullptr;
        }
        if(sensor!=nullptr){
            sensor->Close();
            sensor->Release();
            sensor=nullptr;
        }
    };
    try{
        const auto ndim=::mxGetNumberOfDimensions(src);
        const auto& dim=::mxGetDimensions(src);
        if(ndim!=2||dim[1]!=3)
            throw runtime_error(syntax_error_message);
        vector<CameraSpacePoint> camera_space_points;
        if     (::mxIsSingle(src)) camera_space_points=to_camera_space_points<float>   (src);
        else if(::mxIsDouble(src)) camera_space_points=to_camera_space_points<double>  (src);
        else if(::mxIsInt64(src))  camera_space_points=to_camera_space_points<int64_t> (src);
        else if(::mxIsUint64(src)) camera_space_points=to_camera_space_points<uint64_t>(src);
        else if(::mxIsInt32(src))  camera_space_points=to_camera_space_points<int32_t> (src);
        else if(::mxIsUint32(src)) camera_space_points=to_camera_space_points<uint32_t>(src);
        else if(::mxIsInt16(src))  camera_space_points=to_camera_space_points<int16_t> (src);
        else if(::mxIsUint16(src)) camera_space_points=to_camera_space_points<uint16_t>(src);
        else if(::mxIsInt8(src))   camera_space_points=to_camera_space_points<int8_t>  (src);
        else if(::mxIsUint8(src))  camera_space_points=to_camera_space_points<uint8_t> (src);
        else
            throw runtime_error("unsupported data type");

        if(FAILED(::GetDefaultKinectSensor(&sensor))||
           FAILED(sensor->Open())||
           FAILED(sensor->get_CoordinateMapper(&coordinate_mapper))||
           FAILED(sensor->OpenMultiSourceFrameReader(FrameSourceTypes_Depth|FrameSourceTypes_Color,&multisource_frame_reader)))
        {
            throw runtime_error("failed to get a default kinect sensor");
        }
        static auto wait_start=chrono::high_resolution_clock::now();
        wait_start=chrono::high_resolution_clock::now();
		while(1){
			if(SUCCEEDED(multisource_frame_reader->AcquireLatestFrame(&multisource_frame)))
				break;
			if(chrono::high_resolution_clock::now()-wait_start>chrono::seconds(10))
				throw runtime_error("failed to acquire a kinect multisource frame");
		}
		if(FAILED(multisource_frame->get_DepthFrameReference(&depth_frame_reference)))
			throw runtime_error("failed to get a kinect depth frame reference");
		if(FAILED(depth_frame_reference->AcquireFrame(&depth_frame)))
			throw runtime_error("failed to acquire a kinect depth frame");
		if(FAILED(multisource_frame->get_ColorFrameReference(&color_frame_reference)))
			throw runtime_error("failed to get a kinect color frame reference");
		if(FAILED(color_frame_reference->AcquireFrame(&color_frame)))
			throw runtime_error("failed to acquire a kinect color frame");

        /* if(FAILED(sensor->get_CoordinateMapper(&coordinate_mapper)))
            throw runtime_error("failed to get a kinect coordinate mapper"); */

        const auto& nb_points=camera_space_points.size();
        vector<ColorSpacePoint> color_space_points(nb_points);
        /* transform(
            camera_space_points.cbegin(),
            camera_space_points.cend(),
            color_space_points.begin(),
            [](const CameraSpacePoint& p)->ColorSpacePoint{
                return {p.X,p.Y};
            }); */
        /* if(FAILED(coordinate_mapper->MapCameraPointsToColorSpace(
            UINT(nb_points),
            camera_space_points.data(),
            UINT(nb_points),
            color_space_points.data())))
        {
            throw runtime_error("failed to map camera points to color space");
        } */
        transform(
            camera_space_points.cbegin(),
            camera_space_points.cend(),
            color_space_points.begin(),
            [&](const CameraSpacePoint& p)->ColorSpacePoint{
                ColorSpacePoint result;
                if(FAILED(coordinate_mapper->MapCameraPointToColorSpace(p,&result)))
                    throw runtime_error("failed to map a camera point to the color space");
                return result;
            });

        mxArray *result=nullptr;
        if     (::mxIsSingle(src)) result=from_color_space_points<float>   (color_space_points);
        else if(::mxIsDouble(src)) result=from_color_space_points<double>  (color_space_points);
        else if(::mxIsInt64(src))  result=from_color_space_points<int64_t> (color_space_points);
        else if(::mxIsUint64(src)) result=from_color_space_points<uint64_t>(color_space_points);
        else if(::mxIsInt32(src))  result=from_color_space_points<int32_t> (color_space_points);
        else if(::mxIsUint32(src)) result=from_color_space_points<uint32_t>(color_space_points);
        else if(::mxIsInt16(src))  result=from_color_space_points<int16_t> (color_space_points);
        else if(::mxIsUint16(src)) result=from_color_space_points<uint16_t>(color_space_points);
        else if(::mxIsInt8(src))   result=from_color_space_points<int8_t>  (color_space_points);
        else if(::mxIsUint8(src))  result=from_color_space_points<int8_t>  (color_space_points);
        else
            throw runtime_error("unsupported data type");
        release_all_interfaces();
        return result;
    }catch(...){
        release_all_interfaces();
        throw;
    }
}
