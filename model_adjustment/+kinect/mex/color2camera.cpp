#include <mex.h>
#include <stdexcept>
#include <string>
#include <chrono>
#include <tuple>
#include <vector>
#include <functional>
#include <algorithm>

#include <Kinect.h>
#pragma comment (lib,"kinect20.lib")

using namespace std;

const auto syntax_error_message=string("the function argument must be a scalar value");

mxArray* color_space_points_to_camera_space_points(const function<void(vector<UINT16>&)>& f);

void mexFunction(int nb_outputs,mxArray *outputs[],int nb_inputs,const mxArray *inputs[]){
    try{
        if(nb_inputs==0){
            outputs[0]=color_space_points_to_camera_space_points([](vector<UINT16>&){});
            return;
        }else if(nb_inputs!=1||!::mxIsNumeric(inputs[0])||!::mxIsScalar(inputs[0]))
            throw runtime_error(syntax_error_message);
        const auto depth=::mxGetScalar(inputs[0]);
        outputs[0]=color_space_points_to_camera_space_points([&depth](vector<UINT16>& buffer){
            fill(buffer.begin(),buffer.end(),UINT16(depth*1000.0));
        });
    }
    catch(const exception& e){
        const auto& function_name=::mexFunctionName();
        mexErrMsgIdAndTxt("MATLAB:mexfunction:inputOutputMismatch",e.what());
    }
    catch(...){
        const auto& function_name=::mexFunctionName();
        mexErrMsgIdAndTxt("MATLAB:mexfunction:inputOutputMismatch","unhandled exception");
    }
}

mxArray* color_space_points_to_camera_space_points(const function<void(vector<UINT16>&)>& f){
    IKinectSensor           *sensor                  =nullptr;
    ICoordinateMapper       *coordinate_mapper       =nullptr;
    IMultiSourceFrameReader *multisource_frame_reader=nullptr;
	IMultiSourceFrame       *multisource_frame       =nullptr;
	IColorFrameReference    *color_frame_reference   =nullptr;
	IColorFrame             *color_frame             =nullptr;
	IDepthFrameReference    *depth_frame_reference   =nullptr;
	IDepthFrame             *depth_frame             =nullptr;
    const auto& release_all_interfaces=[&](){
		if(depth_frame!=nullptr){
			depth_frame->Release();
			depth_frame=nullptr;
		}
		if(depth_frame_reference!=nullptr){
			depth_frame_reference->Release();
			depth_frame_reference=nullptr;
		}
		if(color_frame!=nullptr){
			color_frame->Release();
			color_frame=nullptr;
		}
		if(color_frame_reference!=nullptr){
			color_frame_reference->Release();
			color_frame_reference=nullptr;
		}
		if(multisource_frame!=nullptr){
			multisource_frame->Release();
			multisource_frame=nullptr;
		}
        if(multisource_frame_reader!=nullptr){
            multisource_frame_reader->Release();
            multisource_frame_reader=nullptr;
        }
        if(coordinate_mapper!=nullptr){
            coordinate_mapper->Release();
            coordinate_mapper=nullptr;
        }
        if(sensor!=nullptr){
            sensor->Close();
            sensor->Release();
            sensor=nullptr;
        }
    };
    try{
        if(FAILED(::GetDefaultKinectSensor(&sensor))||
           FAILED(sensor->Open())||
           FAILED(sensor->get_CoordinateMapper(&coordinate_mapper))||
           FAILED(sensor->OpenMultiSourceFrameReader(FrameSourceTypes_Depth|FrameSourceTypes_Color,&multisource_frame_reader)))
        {
            throw runtime_error("failed to get a default kinect sensor");
        }
        static auto wait_start=chrono::high_resolution_clock::now();
        wait_start=chrono::high_resolution_clock::now();
		while(1){
			if(SUCCEEDED(multisource_frame_reader->AcquireLatestFrame(&multisource_frame)))
				break;
			if(chrono::high_resolution_clock::now()-wait_start>chrono::seconds(10))
				throw runtime_error("failed to acquire a kinect multisource frame");
		}
		if(FAILED(multisource_frame->get_DepthFrameReference(&depth_frame_reference)))
			throw runtime_error("failed to get a kinect depth frame reference");
		if(FAILED(depth_frame_reference->AcquireFrame(&depth_frame)))
			throw runtime_error("failed to acquire a kinect depth frame");
        vector<UINT16> depth_buffer;
        {UINT   depth_size=0;
        UINT16 *depth_data=nullptr;
        if(FAILED(depth_frame->AccessUnderlyingBuffer(&depth_size,&depth_data)))
            throw runtime_error("failed to access the kinect depth frame data");
        depth_buffer.resize(size_t(depth_size));
		if(FAILED(depth_frame->CopyFrameDataToArray(depth_size,depth_buffer.data())))
			throw runtime_error("failed to copy the kinect depth frame data");
        }
        f(depth_buffer);
		if(FAILED(multisource_frame->get_ColorFrameReference(&color_frame_reference)))
			throw runtime_error("failed to get a kinect color frame reference");
		if(FAILED(color_frame_reference->AcquireFrame(&color_frame)))
			throw runtime_error("failed to acquire a kinect color frame");

        const auto& nb_points=size_t(1920*1080);
        vector<CameraSpacePoint> camera_space_points(nb_points);
        if(FAILED(coordinate_mapper->MapColorFrameToCameraSpace(
            UINT(depth_buffer.size()),
            depth_buffer.data(),
            UINT(nb_points),
            camera_space_points.data())))
        {
            throw runtime_error("failed to map color space points to camera space");
        }

        const mwSize dim[2]={mwSize(nb_points),3};
        const auto result=::mxCreateNumericArray(2,dim,mxDOUBLE_CLASS,mxREAL);
        auto itx=(double*)::mxGetData(result);
        auto ity=itx+nb_points;
        auto itz=ity+nb_points;
        for_each(camera_space_points.cbegin(),camera_space_points.cend(),[&](const CameraSpacePoint& p){
            *itx++=p.X;
            *ity++=p.Y;
            *itz++=p.Z;
        });
        release_all_interfaces();
        return result;
    }catch(...){
        release_all_interfaces();
        throw;
    }
}
