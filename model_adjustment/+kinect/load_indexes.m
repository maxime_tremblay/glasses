function out = load_indexes(filename, K)

out = csvread(filename);
if nargin>=2
    out = out(1:K);
end