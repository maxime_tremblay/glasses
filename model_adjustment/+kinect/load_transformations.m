function out=load_transformations(file, k, mirrored)
%LOAD_TRANSFORMATIONS Read the .transforms file containing all the
%camera extrinsic parameters
%   OUT = LOAD_TRANSFORMATIONS(FILE) Reads the FILE.transforms file and
%   returns all the transformations.
%
%   OUT = LOAD_TRANSFORMATIONS(FILE, K) Reads the FILE.transforms file and
%   return K transformations.
%
%   OUT = LOAD_TRANSFORMATIONS(FILE, K, MIRRORED) Reads the FILE.transforms
%   file and returns K transformations transposed. If K == [], returns all
%   transformations.
%
%   Copyright 2016 LVSN
if nargin<3
    mirrored = true;
end

file_id=fopen(file,'r');
if(file_id<0)
    error(['failed to open file "' file '"']);
end

try
    
    if nargin>1 || isempty(k)
        nb_frames = k;
        out=zeros(4,4,nb_frames);
    else 
        nb_frames = inf;
    end
    
    i = 1;
    while ~feof(file_id) && i<=nb_frames
        if mod(i,10)==0
            tic
        end
        for j=1:4
            tline = fgetl(file_id);
            out(j,:,i) = str2double(strsplit(tline,' '));
        end
        fgetl(file_id); fgetl(file_id);
        if mod(i,10)==0
            toc
        end
        i=i+1;
    end
    fclose(file_id);
    
    if ~mirrored
        for i=1:size(out,3)
            out(:,:,i) = out(:,:,i)';
        end
    end
catch exception
    fclose(file_id);
    rethrow(exception);
end
return;