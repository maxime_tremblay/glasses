function out=plane_target(M,N,dx,dy)

out=zeros(M*N,3);
for row=1:M
    y=dy*(row-1);
    row_offset=(row-1)*N;
    for col=1:N
        x=dx*(col-1);
        out(row_offset+col,:)=[y x 0];
    end
end
return;