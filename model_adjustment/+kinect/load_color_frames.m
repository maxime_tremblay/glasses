function [outrgb, outdepth] = load_color_frames(file,k)
%LOAD_COLOR_FRAMES Read the .frames file containing all the images from 
% a modelization session (color and depth images).
%
%   OUTRGB = LOAD_COLOR_FRAMES(FILE) Read the FILE.frames and return all
%   the rgb images in OUTRGB. It does not read the deapth images.
%
%   [OUTRGB, OUTDEPTH] = LOAD_COLOR_FRAMES(FILE) Read the FILE.frames and
%   return all the rbg images in OUTRGB and all the depth images in
%   OUTDEPTH.
%
%   [OUTRGB, OUTDEPTH] = LOAD_COLOR_FRAMES(FILE, K) Same as previous
%   function iterations, but return only K images.
%
%   Copyright 2016 LVSN

file_id=fopen(file,'r');
if(file_id<0)
    error(['failed to open file "' file '"']);
end
try
    color_frame_width =1920;
    color_frame_height=1080;
    header=fread(file_id,4,'char=>char');
    assert(strcmp(header','LVSN'));
    nb_frames=fread(file_id,1,'uint64=>uint64');
    if nargin>1
        K = min(k,nb_frames);
    else
        K = nb_frames;
    end
    outrgb=zeros(color_frame_height,color_frame_width,3,K,'uint8');
    for i=1:K
        if mod(i,10)==0
            tic
        end
        img=fread(file_id,color_frame_height*color_frame_width*4,'uint8=>uint8');
        outrgb(:,:,:,i) = permute(reshape([img(3:4:end), img(2:4:end), img(1:4:end)], color_frame_width, color_frame_height, 3),[2 1 3]);
        if mod(i,10)==0
            toc
        end
    end
    if nargout==2
        
        offset = nb_frames*color_frame_height*color_frame_width*4 + 4;
        color_frame_width  = 512;
        color_frame_height = 424;
        outdepth = zeros(color_frame_height, color_frame_width, K, 'uint16');
        fseek(file_id, offset, 'bof');
        for i=1:K
            if mod(i,10)==0
                tic
            end
            img = fread(file_id, color_frame_height*color_frame_width,'uint16=>uint16');
            outdepth(:,:,i) = reshape(img, color_frame_width, color_frame_height)';
            if mod(i,10)==0 
                toc
            end
        end
    end
    fclose(file_id);
catch exception
    fclose(file_id);
    rethrow(exception);
end
outrgb = fliplr(outrgb);
outdepth = fliplr(outdepth);
return;
