%deform_test
close all
addpath(genpath('./data/'))
addpath('../3rdparty/plyread/');
addpath('../3rdparty/sovarux/');
addpath('../3rdparty/SpinCalc/');

if ~exist('face','var')
    fa = '003_voxel_box_768_angle_80';
    filename = [fa '.mat'];
    if ~exist(filename,'file')
        face = load_data('003_voxel_box_768_angle_80.ply', ...
            {'003_voxel_box_768_angle_80_nosearc.ply', ...
            '003_voxel_box_768_angle_80_leftear.ply', ...
            '003_voxel_box_768_angle_80_rightear.ply'});
        save(filename,'face');
    else
        load(filename,'face');
    end
end
gl = 'glasses_nolens';
filename = [gl '.mat'];
scales = 0.00075:0.00025:0.00175;
for i=scales
    for j=scales
        if ~exist(filename,'file')
            glasses = load_data('glasses_nolens.ply',{'glasses_nosearc_2.ply', ... 
                'glasses_leftside_2.ply', 'glasses_rightside_2.ply'});
            glasses_parts = load_data('glasses_nolens.ply', ...
                {'glasses_nosearc_2.ply', 'glasses_leftbranch.ply', 'glasses_rightbranch.ply'});
            save(filename,'glasses','glasses_parts');
        else
            load(filename,'glasses','glasses_parts');
        end
        s = [i j 1/1000];
        glasses = rescale_model(glasses,s);
        glasses_parts = rescale_model(glasses_parts,s);
        glasses.vertex = bsxfun(@minus,glasses.vertex,[0 0 0.8]); glasses_parts.vertex = bsxfun(@minus,glasses_parts.vertex,[0 0 0.8]);


        [projected_glasses, ER, R, T] = model_adjustment(face, glasses);

        r(1) = struct('axis',{{34177,[0 1 0]}},'label',2); 
        r(2) = struct('axis',{{352,[0 -1 0]}},'label',1); 


        variation = inf; tol = 0.001; %?
        opt_theta = 0;
        nstep = 0; 
        nstep_max = 10;
        while(variation > tol && nstep < nstep_max)
            fprintf('theta: %2.4f\n', opt_theta);

            deformed_model = deform(opt_theta, glasses_parts, r);
            aligned_vertex = bsxfun(@plus,R*deformed_model.vertex',T)';

            kdOBJ = KDTreeSearcher(aligned_vertex);
            [match, mindist] = knnsearch(kdOBJ, face.vertex);

            o = @(t) optimize_deformation( t, face, glasses_parts, match, R, T, r);
            [tmp_theta, resnorm] = lsqnonlin(o, opt_theta, [], [], optimoptions('lsqnonlin','Display','off'));
            variation = abs(opt_theta-tmp_theta); opt_theta = tmp_theta;
            fprintf('variation: %2.4f, resnorm: %2.4f, nstep %d\n', variation,resnorm,nstep);

            nstep = nstep+1;
        end

        variation = inf; tol = 0.01; %?
        Q = SpinCalc('DCMtoQ',R, 0.01, 1);
        params = [opt_theta Q T'];
        nstep = 0;
        nstep_max = 10;
        while(variation > tol && nstep < nstep_max)
            angles = SpinCalc('QtoEA321',params(2:5), 0.1, 0);
            yaw = angles(1); pitch = angles(2); roll = angles(3);
            fprintf('theta: %2.4f, rotation (yaw-pitch-roll): %2.2f, %2.2f, %2.2f, translation: %2.2f, %2.2f, %2.2f\n', [params(1), yaw, pitch, roll, params(6:8)]);

            deformed_model = deform(params(1), glasses_parts, r);
            R = SpinCalc('QtoDCM',params(2:5), 0.1, 0);
            T = params(6:8)';
            aligned_vertex = bsxfun(@plus,R*deformed_model.vertex(deformed_model.labels~=-1,:)',T)';
            kdOBJ = KDTreeSearcher(aligned_vertex);
            [match, mindist] = knnsearch(kdOBJ,face.vertex(face.labels~=-1,:));

            o = @(p) optimize_globaltrans( p, face, glasses_parts, match, r);
            [tmp_params, resnorm] = lsqnonlin(o, params, [], [], optimoptions('lsqnonlin','Algorithm','Levenberg-Marquardt','Display','off'));
            variation = sqrt(sum((params-tmp_params).^2)); params = tmp_params;

            angles = SpinCalc('QtoEA321',params(2:5), 0.1, 0);
            yaw = angles(1); pitch = angles(2); roll = angles(3);

            nstep = nstep+1;
            fprintf('variation: %2.4f, resnorm: %2.4f, nstep: %d\n', variation, resnorm, nstep);
        end
        % 
        deformed_model = deform(params(1),glasses_parts,r);
        R = SpinCalc('QtoDCM',params(2:5), 0.1, 0);
        T = params(6:8)';
        projected_deformed = deformed_model; projected_deformed.vertex = bsxfun(@plus,R*projected_deformed.vertex',T)';
        
        img_filename = sprintf('results/%s_%s_%2.4f_%2.4f.png',fa,gl,i,j);
        save_model(img_filename,face,projected_deformed)
    end
end