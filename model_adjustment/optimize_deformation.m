function err = optimize_deformation(theta, obj1, obj2, match, R, T, r)

deformed_model = deform(theta,obj2,r);

err = [];
labels = unique(deformed_model.labels)'; labels = labels(labels>=0);
for i=1:length(labels)
    projected_vertex = bsxfun(@plus,R*deformed_model.vertex(deformed_model.labels==labels(i) & ~any(isnan(deformed_model.normal),2),:)',T)';
    err = cat(1,err,projected_vertex(match{i},:)-obj1.vertex(obj1.labels==labels(i) & ~any(isnan(obj1.normal),2),:));
end