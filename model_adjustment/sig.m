function v = sig(t,a,b)
%SIG Homemade sigmoid function
%   V = SIG(T) Return V, the response to the sigmoid function sig(x) =
%   1/(1+exp(-t)).
%
%   V = SIG(T, A) Return V, the response to the sigmoid function sig(x) =
%   1/(1+exp(-t*a)). The higher is A, the more steep the sigmoid will be.
%
%   V = SIG(T, A, B) Return V, the response to the sigmoid function 
%   sig(x) = 1/(1+exp(-t*a+b)). The value B translate the sigmoid (positive
%   right, negative left)
%
%   2016 Maxime Tremblay
if nargin<2
    a=1;
end
if nargin<3
    b=0;
end
v = 1./(1+exp(-t*a+b));