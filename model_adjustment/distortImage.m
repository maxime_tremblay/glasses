function img = distortImage(img, camera, varargin)
% Applies the radial distortion parameters estimated by SfM to an image. 
%
%   img = distortImage(img, camera)
%
% See also:
%   undistortImage
%
% ----------
% Jean-Francois Lalonde

undistort = false;
interpMethod = 'linear';

if undistort
    fn = @times;
else
    fn = @rdivide;
end

% Simulate camera's radial distortion here!
if isfield(camera, 'radialDistortion')
    [nrows, ncols, ~] = size(img);
    
    % we'll need to determine if the image has been resized (and by how
    % much)
    f = camera.focalLength .* ((ncols/2) ./ camera.principalPoint.u0);
    
    [x,y] = meshgrid(1:ncols, 1:nrows);
    xNew = x - ncols/2;
    yNew = y - nrows/2;
    r2 = (xNew.^2 + yNew.^2) ./ f.^2;
    
    factor = 1 + camera.radialDistortion.k0 * r2 + ...
        camera.radialDistortion.k1 * r2.^2;
    
    xNew = fn(xNew, factor);
    yNew = fn(yNew, factor);
    
    xNew = xNew + ncols/2;
    yNew = yNew + nrows/2;
    
    % interpolate. Use nn interpolation to avoid creating new values.
    for i_c = 1:size(img, 3)
        img(:,:,i_c) = interp2(x, y, img(:,:,i_c), xNew, yNew, interpMethod);
    end
end