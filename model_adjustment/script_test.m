%% Init
close all
addpath(genpath('./data/'))
addpath('../3rdparty/plyread/');
%%  Load data test 1

face = plyread('00001_20061015_00418_neutral_face05.ply');
face_tri = triangulation(vertcat(face.face.vertex_indices{:})+1, face.vertex.x, face.vertex.y, face.vertex.z);
h1 = figure; trisurf(face_tri);
glasses = plyread('glasses.ply');
glasses_tri = triangulation(vertcat(glasses.face.vertex_indices{:})+1, glasses.vertex.x, glasses.vertex.y, glasses.vertex.z);
h2 = figure; trisurf(glasses_tri);

face_face_vertex = vertcat(face.face.vertex_indices{:})+1;
face_vertex = [face.vertex.x, face.vertex.y, face.vertex.z];

clear face face_tri

glasses_face_vertex = vertcat(glasses.face.vertex_indices{:})+1;
glasses_vertex = [glasses.vertex.x, glasses.vertex.y, glasses.vertex.z];

clear glasses glasses_tri

%% Point selection (manualy selected in meshlab) face
face_leftear = plyread('00001_20061015_00418_neutral_face05_leftear_2.ply');
face_rightear = plyread('00001_20061015_00418_neutral_face05_rightear_2.ply');
face_nosearc = plyread('00001_20061015_00418_neutral_face05_nosearc_2.ply');

face_feat_vertex = [face_leftear.vertex.x, face_leftear.vertex.y, face_leftear.vertex.z; 
    face_rightear.vertex.x, face_rightear.vertex.y, face_rightear.vertex.z; 
    face_nosearc.vertex.x, face_nosearc.vertex.y, face_nosearc.vertex.z];

rface = [ones(1,length(face_leftear.vertex.x))*0,ones(1,length(face_rightear.vertex.x)),ones(1,length(face_nosearc.vertex.x))*2];

clear face_leftear face_rightear face_nosearc

%% Point selection (manualy selected in meshlab) glasses
glasses_leftside = plyread('glasses_leftside_2.ply');
glasses_rightside = plyread('glasses_rightside_2.ply');
glasses_nosearc = plyread('glasses_nosearc_2.ply');

glasses_feat_vertex = [glasses_leftside.vertex.x, glasses_leftside.vertex.y, glasses_leftside.vertex.z; 
    glasses_rightside.vertex.x, glasses_rightside.vertex.y, glasses_rightside.vertex.z; 
    glasses_nosearc.vertex.x, glasses_nosearc.vertex.y, glasses_nosearc.vertex.z];

rglasses = [ones(1,length(glasses_leftside.vertex.x))*0,ones(1,length(glasses_rightside.vertex.x)),ones(1,length(glasses_nosearc.vertex.x))*2];

clear glasses_leftside glasses_rightside glasses_nosearc

%% Show both model in the same figure (find scale param)
s = 1000; %mm to m
S = [s 0 0; 0 s 0; 0 0 s]; T = -mean(glasses_vertex); TS = -s*T;
figure; plot3(face_vertex(:,1), face_vertex(:,2), face_vertex(:,3),'.b'); hold on; plot3(glasses_vertex(:,1),glasses_vertex(:,2),glasses_vertex(:,3),'.r');
scaled_glasses_vertex = ([glasses_vertex(:,1),glasses_vertex(:,2),glasses_vertex(:,3)]+repmat(T,size(glasses_vertex(:,1),1),1))*S+repmat(TS,size(glasses_vertex(:,1),1),1);
scaled_glasses_feat_vertex = ([glasses_feat_vertex(:,1),glasses_feat_vertex(:,2),glasses_feat_vertex(:,3)]+repmat(T,size(glasses_feat_vertex(:,1),1),1))*S+repmat(TS,size(glasses_feat_vertex(:,1),1),1);
figure; plot3(face_vertex(:,1), face_vertex(:,2), face_vertex(:,3),'.b'); hold on; plot3(scaled_glasses_vertex(:,1), scaled_glasses_vertex(:,2), scaled_glasses_vertex(:,3), '.r');


%% ICP (point by point)
addpath('../3rdparty/icp/');
q = face_feat_vertex;
p = scaled_glasses_feat_vertex;

[TR,TT] = icp(q',p','Matching','kDtree');

%% With Weight function
w = @(idx) weight(idx,rface,rglasses);
[TR,TT] = icp(q',p','Matching','kDtree','Weight',w);


%% Test bidon
qp = bsxfun(@plus,TR*p',TT);
figure; plot3(qp(1,:),qp(2,:),qp(3,:),'.r');
hold on;
plot3(q(:,1),q(:,2),q(:,3),'.b');

%% Project the glasses on the face!

projected_glasses_vertex = (TR*scaled_glasses_vertex' + repmat(TT,1,size(scaled_glasses_vertex,1)))';
figure; plot3(face_vertex(:,1), face_vertex(:,2), face_vertex(:,3),'.b'); hold on; plot3(projected_glasses_vertex(:,1), projected_glasses_vertex(:,2), projected_glasses_vertex(:,3), '.r');

glasses_tri = triangulation(glasses_face_vertex, projected_glasses_vertex(:,1), projected_glasses_vertex(:,2), projected_glasses_vertex(:,3));
face_tri = triangulation(face_face_vertex, face_vertex(:,1), face_vertex(:,2), face_vertex(:,3));
figure; trisurf(face_tri,'EdgeColor','None'); hold on; trisurf(glasses_tri);

%% Yannick surf

addpath('../3rdparty/sovarux/');
mixed_tri = triangulation([glasses_face_vertex;face_face_vertex+length(projected_glasses_vertex(:,1))],[projected_glasses_vertex(:,1);face_vertex(:,1)], [projected_glasses_vertex(:,2);face_vertex(:,2)], [projected_glasses_vertex(:,3);face_vertex(:,3)]);
figure; pgShowSurface3( mixed_tri );

%% not mixed!

addpath('../3rdparty/sovarux/');
glasses_tri = triangulation(glasses_face_vertex,projected_glasses_vertex(:,1),projected_glasses_vertex(:,2),projected_glasses_vertex(:,3));
face_tri = triangulation(face_face_vertex,face_vertex(:,1),face_vertex(:,2),face_vertex(:,3));
figure; hold on;
pgShowSurface3( face_tri ); pgShowSurface3( glasses_tri, struct('color', [0.15 0.15 0.15]) );
%[axs,hsurf,hplot] = pgShowSurface3( face_tri );
%pgShowSurface3( glasses_tri, struct('color', [0.15 0.15 0.15], 'hplot', hplot) );
hold off