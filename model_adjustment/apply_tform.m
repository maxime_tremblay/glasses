function model = apply_tform(model, tform)
%APPLY_TFORM Apply a transform to vertices
%   MODEL = APPLY_TFORM(MODEL, TFORM) Apply a transform TFORM (4x4 matrix)
%   to vertices from the MODEL struct VERTEX field.
%
%   2016 Maxime Tremblay

assert(size(T)==[4 4]);
vertex = [model.vertex ones(size(model.vertex,1),1)]*tform;
vertex = bsxfun(@times,vertex,vertex(:,4));
model.vertex = vertex(:,1:3);
