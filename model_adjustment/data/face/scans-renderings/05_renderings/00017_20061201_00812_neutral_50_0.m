%% image dimensions
width=768;
height=1024;

%% [right, left, bottom, top, near, far] OpenGL Frustum
frustum=[0.075,-0.075,-0.1,0.1,0.4,8];

%% 4x4 homogeneous 3D translation and rotation 
T = [4.84065e-06 0 -5.76886e-06 0.485763;0 7.53071e-06 0 -0.422473;5.76886e-06 0 4.84065e-06 -4.92003;0 0 0 1];

%% structure with lights 
light(1) = struct('ambient',[0.4,0.4,0.4,1],'diffuse',[0.5,0.5,0.5,1],'specular',[0.5,0.5,0.5,1],'dir',[       0,      0.5, 0.866025]);

%% materials 
mat(1) = struct('name','mat_0__front','ambient',[1,1,1,1],'diffuse',[1,1,1,1],'specular',[0.12,0.12,0.12,1],'phong_exp',40);
