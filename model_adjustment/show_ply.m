function show_ply(filename, savefile)
% SHOW_PLY open a ply file and shows it
%   
%   SHOW_PLY(FILENAME) Open a plyfile FILENAME and shows it in an
%   interactive figure.
%
%   SHOW_PLY(FILENAME, SAVEFILE) Open a plyfile FILENAME, shows it in an
%   interactive figure and also save a png image with the name SAVEFILE.
%
%   2016 Maxime Tremblay

model = load_data(filename);
if nargin==2
    save_model(savefile, model);
else
    save_model([], model, [], true);
end