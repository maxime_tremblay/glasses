function e = optimize_globaltrans_clip(params, obj1, obj2, match, lambda, r)

beta = params(1);
R = SpinCalc('QtoDCM', params(2:5), 0.1, 0);
T = params(6:8)';

deformed_model = deform(beta,obj2,r);
err1 = []; err2 = [];
labels = unique(deformed_model.labels)'; labels = labels(labels>=0);
for i=1:length(labels)
    projected_vertex = bsxfun(@plus,R*deformed_model.vertex(deformed_model.labels==labels(i) & ~any(isnan(deformed_model.normal),2),:)',T)';
    nok = obj1.labels==labels(i) & ~any(isnan(obj1.normal),2);
    err1 = cat(1,err1,projected_vertex(match{i},:)-obj1.vertex(nok,:));
    
    %only for the nose region!
    if labels(i)==0
        tmp = projected_vertex(match{i},:);
        d = tmp-obj1.vertex(nok,:); d = d./repmat(sqrt(sum(d.^2,2)),1,3);
        %the shape of this sigmoid penalise strongly clipped model
        err2 = cat(1,err2,1-sig(sum(d.*obj1.normal(nok,:),2),10,-5));
    end
end

e = sum(sum(err1.^2,2))+mean(err2.^2*lambda);