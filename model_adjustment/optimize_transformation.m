function err = optimize_transformation(params, obj1, obj2, match)

R = SpinCalc('QtoDCM', params(1:4), 0.1, 0);
T = params(5:7)';

err = [];
labels = unique(obj2.labels)'; labels = labels(labels>=0);
for i=1:length(labels)
    projected_vertex = bsxfun(@plus,R*obj2.vertex(obj2.labels==labels(i) & ~any(isnan(obj2.normal),2),:)',T)';
    err = cat(1,err,projected_vertex(match{i},:)-obj1.vertex(obj1.labels==labels(i) & ~any(isnan(obj1.normal),2),:));
end