function save_model(filename, face, glasses, title_str, show)
% SAVE_MODEL function to save 3D model(s) in a 2D image format
%
%   SAVE_MODEL(FILENAME, FACE, GLASSES) Save a FILENAME png image of the 
%   FACE and GLASSES 3D model. The function takes care to add empty space 
%   which is [240, 240, 240] an alpha value of 1.
%
%   SAVE_MODEL(FILENAME, FACE, GLASSES, TITLE_STR) As above, but also
%   add a title TITLE_STR to the saved figure.
%
%   SAVE_MODEL(FILENAME, FACE, GLASSES, TITLE_STR, SHOW) As above, but also
%   show the result in a figure.
%
%   2016 Maxime Tremblay

h = show_adjusted_glasses(face,glasses);
if nargin >= 4 && ~isempty(title_str)
    title(title_str);
end
if nargin < 5 
    show = false;
end

res = get(0, 'screensize'); pos = [0 0 res(3) res(4)]; set(h, 'Position', pos);
img = getframe(h);
vec_img = reshape(img.cdata,[],3); 
alpha = 1-double(reshape(all(bsxfun(@eq,vec_img,[240  240  240]),2),size(img.cdata,1),size(img.cdata,2)));

if ~isempty(filename)
    imwrite(img.cdata, filename, 'Alpha', alpha, 'Background', [0 0 0]);
end
if ~show
    close(h);
end