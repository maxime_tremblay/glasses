function new_img = visualize(F, R, T, glasses, face, img, lightpos, sidebyside, show)
%VISUALIZE Function to project the glasses model on an image considering
%the translation and rotation of the camera at this frame.
%	NEW_IMG = VISUALIZE(F, R, T, GLASSES, FACE, IMG) F is the focal lenght 
%   in pixel of the sensor, R the rotation of the camera (3x3 matrix), T 
%   its translation (1x3 vector), GLASSES and FACE are structs containing 
%   vertex and vertex_indices to project in the image IMG.
%
%   NEW_IMG = VISUALIZE(F, R, T, GLASSES, FACE, IMG, LIGHTPOS) LIGHTPOS is
%   a 1x3 vector containing the position of the infinitly distant point
%   light source (default is [0 0 -1]).
%
%   NEW_IMG = VISUALIZE(F, R, T, GLASSES, FACE, IMG, LIGHTPOS, SIDEBYSIDE)
%   SIDEBYSIDE is a bool parameter (default at false) which make the
%   function not project on the image but put the projection on another 
%   empty canvas, side by side with the image so both the image and the 
%   moving model could be observe simultaneously. 
%
%   NEW_IMG = VISUALIZE(F, R, T, GLASSES, FACE, IMG, LIGHTPOS, ...
%   SIDEBYSIDE, SHOW) SHOW is a bool parameter (default at false) which
%   make the function show every visualization on a figure. This is
%   generally only use for debugging purpose.
%
%   Copyright 2016 LVSN


if nargin<7 || isempty(lightpos)
    lightpos = [0 0 -1];
end
if nargin<8
    sidebyside = false;
end
if nargin<9
    show = false;
end
cameraDir = R*[0 0 1]';
cameraUpVector = R*[0 1 0]'; 
cameraPos = T;
if ~sidebyside
    h = figure(1);
    glasses_color = [1 1 1]; face_color = [0 0 0]; 
    if ~isempty(face)
        show_object(face,face_color);
        hold on;
    end
    if ~isempty(glasses)
        show_object(glasses,glasses_color);
    end
    hold off;
    lighting none
    set_camera_position(h,F,cameraPos,cameraDir,cameraUpVector,[size(img,1),size(img,2)]);
    frame = getframe(h); cdata = reshape(frame.cdata(:,:,1),[size(frame.cdata,1),size(frame.cdata,2)]);
    %close(h);
    mask = min(imresize(cdata,[size(img,1),size(img,2)],'nearest'),1);
end


h = figure(1);
if ~isempty(face)
    show_object(face, min(1, 170 * [ 1.5 1.15 1.0 ]/255), lightpos);
    hold on;
end
if ~isempty(glasses)
    if ~isempty(face)
        show_object(glasses, [0.75 0.75 0.75]);
    else
        show_object(glasses, [0.75 0.75 0.75], lightpos);
    end
end
hold off;
set_camera_position(h,F,cameraPos,cameraDir,cameraUpVector,[size(img,1),size(img,2)]);
frame = getframe(h); cdata = imresize(frame.cdata,[size(img,1),size(img,2)]);

if ~sidebyside
    new_img = reshape(bsxfun(@times,reshape(img,[],size(img,3)),1-mask(:)), size(img)) + reshape(bsxfun(@times,reshape(cdata,[],size(img,3)),mask(:)),size(cdata));
else
    new_img = cat(1,img,cdata);
end
if show
    figure; imshow(new_img);
end