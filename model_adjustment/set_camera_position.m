function set_camera_position(figHandle, focalLength, cameraPos, cameraDir, cameraUpVector, imgDims)
% SET_CAMERA_POSITION Places the camera at the specific position/direction
%
%   SET_CAMERA_POSITION(FIGHANDLE, FOCALLENGHT, CAMERAPOS, CAMERADIR,
%   CAMERAUPVECTOR, IMGDIMS) Put the camera in figure FIGUREHANDLE at 
%   position CAMERAPOS with CAMERADIR direction and CAMERAUPVECTOR up 
%   vector. FOCALLENGHT and IMGDIMS are used to assure the 3D
%   representation fits the images on which it was created.
% ----------
%
% 2015-2016 Jean-Francois Lalonde & Maxime Tremblay
pos = get(figHandle, 'Position');
ax = gca(figHandle);

set(figHandle, 'Renderer', 'opengl', 'Color', 'k');
set(ax, 'CameraUpVector', cameraUpVector, ...
    'CameraPosition', cameraPos, ...
    'CameraTarget', cameraPos + cameraDir, ...
    'CameraViewAngle', atan2(1, focalLength/(max(imgDims)/2))*360/pi, ...
    'DataAspectRatio', [1 1 1], ...
    'PlotBoxAspectRatio', [1 1 1], ...
    'Projection', 'perspective', ...
    'CameraPositionMode', 'manual', ...
    'CameraTargetMode', 'manual', ...
    'CameraUpVectorMode', 'manual', ...
    'CameraViewAngleMode', 'manual', ...
    'Position', [0 0 1 1]);

displayHeight = imgDims(1);
pos(4) = round(displayHeight);
pos(3) = round(pos(4) * imgDims(2)/imgDims(1));
pos(1) = 0; pos(2) = 0;
set(figHandle, 'Position', pos);

posRet = get(figHandle, 'Position');
assert(isequal(pos([3 4]), posRet([3 4])), 'Could not set figure dimensions');

axis('off');


