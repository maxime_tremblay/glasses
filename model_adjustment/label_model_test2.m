%label_model_test
%deform_test
close all
%addpath('./data/2016.02.15 (Frames, Mirrored Mesh, and Transforms)/');
addpath('./data/face/20160324/');
addpath('./data/glasses/');
addpath('../3rdparty/plyread/');
addpath('../3rdparty/sovarux/');
addpath('../3rdparty/SpinCalc/');

saveimg = true;

%% Load mesh data and label feature using the kinect model
name = '013';
face = load_data([name '.ply']); %fichier de fusion
model = load_data([name ' model.ply']);  %fichier du model
%ix = [25, 479, 1065]; %VERY LOW
%ix = [25, 549, 1066]; %LOW
%ix = [25, 142, 1061]; %NORMAL
%ix = [12, 142, 1061]; %NORMAL (lower nose)
ix = [12, 1061, 142]; %NORMAL (lower nose)
face = label_model(face, model, ix, 'K', inf, 'dist', 0.0005);
        
filename = 'glasses_nolens.mat';
if ~exist(filename,'file')
    glasses_parts = load_data('glasses_nolens.ply', ...
        {'glasses_nosearc_2.ply', 'glasses_leftbranch.ply', 'glasses_rightbranch.ply'});
    save(filename,'glasses_parts');
else
   load(filename,'glasses_parts');
end

%% Data manipulation (rescale glasses to be in the same scale as the face)
s = [1/1000 1/1250 1/1000];
glasses_parts = rescale_model(glasses_parts,s);
glasses_parts.vertex = ([-1 0 0;0 1 0;0 0 -1]*glasses_parts.vertex')';
r(1) = struct('axis',{{34177,[0 1 0]}},'label',2); 
r(2) = struct('axis',{{352,[0 -1 0]}},'label',1); 
% show_object(glasses_parts,[0.5 0.5 0.2],[]);
% hold on; show_object(face); hold off;

%% first model adjustment phase (translation) + display
glasses_parts = model_adjustment(face, glasses_parts);
h = show_adjusted_glasses(face, glasses_parts);
if saveimg; imgfilename = [name '_translate.png']; save_model(imgfilename, face, glasses_parts); end;

%% adjust_glasses (transformation only) + display
[params, deformed_model] = adjust_glasses(face, glasses_parts, [], eye(3), [0 0 0]', struct('verbose',true, 'nstep_max',20));
R = SpinCalc('QtoDCM',params(2:5), 0.1, 0);
T = params(6:8)';
h = show_adjusted_glasses(face, deformed_model);
if saveimg; imgfilename = [name '_transform.png'];  save_model(imgfilename, face, deformed_model); end;

%% adjust_glasses (deformation only) + display
[params, deformed_model] = adjust_glasses(face, glasses_parts, r, R, T, struct('deform',true,'trans',false,'init_params',0,'verbose',true));
theta = params(1);
h = show_adjusted_glasses(face, deformed_model);
if saveimg; imgfilename = [name '_deform.png']; save_model(imgfilename, face, deformed_model); end;

%for i=[0 0.0001 0.001 0.01 0.1]
for i = [0 0.1 0.5 1]
%% adjust glasses (deformation + rot + trans + clip check) + display 
lambda = i;
[params, projected_deformed, residual] = adjust_glasses(face, glasses_parts, r, R, T, struct('deform',true,'trans',true,'clip',true,'init_params',theta,'verbose',true,'lambda',lambda,'nstep_max',15,'tol',0.01));
h = show_adjusted_glasses(face, projected_deformed); title(sprintf('lambda = %2.4f',lambda));
if saveimg; imgfilename = [name sprintf('_3views_lambda_%2.4f.png',lambda)]; save_model(imgfilename, face, projected_deformed, sprintf('lambda = %2.4f',lambda)); end;

%% show model clipping through the other model
h = show_clipping(face, projected_deformed, 'xc'); title(sprintf('lambda = %2.4f',lambda));
if saveimg; imgfilename = [name sprintf('_clipping_lambda_%2.4f.png',lambda)]; save_clipping(imgfilename, face, projected_deformed, 'xc', sprintf('lambda = %2.4f',lambda)); end;
end

%%
plywrite(projected_deformed.vertex,projected_deformed.vertex_indices,[name sprintf('_adjusted_lambda_%2.4f.ply',lambda)]);

%%
save([name sprintf('_adjusted_lambda_%2.4f.mat',lambda)],'face','model','projected_deformed','lambda','r','params','residual');