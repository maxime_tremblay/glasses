function model = load_data(mainfile, filenames, rescale)
%LOAD_DATA Reads ply file and creates a struct formated for the 
%model_adjustment toolbox
%
%   MODEL = LOAD_DATA(MAINFILE) Reads the MAINFILE plyfile and creates a
%   MODEL struct formated for the model_adjustment toolbox. This MODEL
%   contains has empty LABEL and LABEL_FILES fields.
%
%   MODEL = LOAD_DATA(MAINFILE, FILENAMES) Use each file inside the cell
%   FILENAMES as a source of label for the MODEL; each vertex in the
%   FILENAMES plyfile must match directly to a vertex in the MAINFILE
%   plyfile.
%
%   MODEL = LOAD_DATA(MAINFILE, [FILENAMES], RESCALE) Same as previous
%   but it also rescale the model of a factor RESCALE.
%
%   2016 Maxime Tremblay

%Need this to read ply files
addpath('../3rdparty/plyread/');

if nargin<2
    filenames = [];
end

if ~iscell(filenames) && ~isempty(filenames)
    filenames = {filenames};
end
tmp_model = struct('vertex',[],'vertex_indices',[],'labels',[],'label_files',{{}});
for i=1:length(filenames)
    filename = filenames{i};
    data = plyread(filename);
    
    offset = size(tmp_model.vertex,1);
    if isfield(data.face,'vertex_indices')
        tmp_model.vertex_indices = vertcat(tmp_model.vertex_indices, vertcat(data.face.vertex_indices{:})+1+offset);
    elseif isfield(data.face,'vertex_index')
        tmp_model.vertex_indices = vertcat(tmp_model.vertex_indices, vertcat(data.face.vertex_index{:})+1+offset);
    end
    
    tmp_model.vertex = [tmp_model.vertex; data.vertex.x, data.vertex.y, data.vertex.z];
    tmp_model.labels = vertcat(tmp_model.labels, ones(length(data.vertex.x),1)*(i-1));
    tmp_model.label_files = vertcat(tmp_model.label_files, filename);
end

model = struct('vertex',[],'vertex_indices',[],'labels',[],'label_files',{{}});
data = plyread(mainfile);
if isfield(data.face,'vertex_indices')
    model.vertex_indices = vertcat(data.face.vertex_indices{:})+1;
elseif isfield(data.face,'vertex_index')
    model.vertex_indices = vertcat(data.face.vertex_index{:})+1;
end

%normals are needed to remove the clipping when adjusting objects
if isfield(data.vertex,'nx') && isfield(data.vertex,'ny') && isfield(data.vertex,'nz')
    model.normal = [data.vertex.nx, data.vertex.ny, data.vertex.nz];
    %normalized
    model.normal = model.normal./repmat(sqrt(sum(model.normal.^2,2)),1,3);
else
    warning('load_data: no normal information in %s', mainfile);
    model.normal = [];
end
model.vertex = [data.vertex.x, data.vertex.y, data.vertex.z];
model.labels = ones(size(model.vertex,1),1)*-1;
model.label_files = tmp_model.label_files;

%this function is a brute force matching of the model loaded from filenames
%to the model loaded from the mainfile; it could takes some time
ix = find_vertex_idx(model.vertex, tmp_model.vertex);
model.labels(ix) = tmp_model.labels;

if nargin>2
    T = -mean(model.vertex);
    model = rescale_model(model, rescale, T);
end