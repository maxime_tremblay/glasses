function [views, rolls] = find_views(model)
%FIND_VIEWS Find axis on which showing the model. Only works on face's 
%generic model.
%
%   [VIEWS, ROLLS] = FIND_VIEWS(MODEL) Look at specific vertex on the
%   generic face MODEL and calculate 3 views orientation by which the face 
%   model could be interesting to visualize.
%
%   Copyright 2016 LVSN

ix = [31, 29, 893];
axe1 = model.vertex(ix(2),:)-model.vertex(ix(1),:); axe1 = axe1/norm(axe1); 
axe2 = model.vertex(ix(3),:)-model.vertex(ix(1),:); axe2 = axe2/norm(axe2);
axe3 = cross(axe1,axe2); axe3 = axe3/norm(axe3);

views = {axe2 , -axe3, -axe2};
rolls = [-90, 180 90];