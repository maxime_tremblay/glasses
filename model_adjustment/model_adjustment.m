%% model_adjustment
% model1 and model2 - 
%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%Output:
% model2 - the adjusted model
% T - the translation to center the model
%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function [model2, R, T] = model_adjustment(model1, model2)

pt1 = mean(model1.vertex(model1.labels~=-1,:));
pt2 = mean(model2.vertex(model2.labels~=-1,:));
T = (pt1-pt2)'; 
model2.vertex = bsxfun(@plus,model2.vertex',T)';
R = eye(3);
