function ix = find_vertex_idx(vertex1, vertex2)
% FIND_VERTEX_IDX function to match two different sets of vertices; useful
% for manualy selecting vertices label.
%
%	IX = FIND_VERTEX_IDX(VERTEX1, VERTEX2) Find all the indices IX of VERTEX2
%	in VERTEX1. This function assert that every vertex should be found.
%
%   2016 Maxime Tremblay

ix = -ones(size(vertex2,1), 1);
for i=1:size(vertex2,1)
    tmp = f(vertex1,vertex2(i,:));
    if ~isempty(tmp)
        ix(i) = tmp;
    end
end
assert(~any(ix==-1),'Every searched vertex should be found and it is not the case.');

function ix = f(vertex,pt)

b = bsxfun(@near_eq,vertex,pt);
ix = find(all(b,2),1);
